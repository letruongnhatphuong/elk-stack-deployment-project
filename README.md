## Automated ELK Stack Deployment

The files in this repository were used to configure the network depicted below.

![Diagram](https://gitlab.com/letruongnhatphuong/elk-stack-deployment-project/-/commit/9e093ad6b104c10bb3f12656139922b402967dc0#note_971646576)


These files have been tested and used to generate a live ELK deployment on Azure. They can be used to either recreate the entire deployment pictured above. Alternatively, select portions of the yml and config file may be used to install only certain pieces of it, such as Filebeat.

  - _TODO: Enter the playbook file._

This document contains the following details:
- Description of the Topologu
- Access Policies
- ELK Configuration
- Beats in Use
- Machines Being Monitored
- How to Use the Ansible Build


### Description of the Topology

The main purpose of this network is to expose a load-balanced and monitored instance of DVWA, the D*mn Vulnerable Web Application.
Load balancing ensures that the application will be highly functional and available, in addition to restricting traffic to the network.

- What aspect of security do load balancers protect?
  - When a server goes down or is otherwise unavailable, load balancers redirect live traffic to another server.

- What is the advantage of a jump box?
  - Having a Jump Box Provisioner also prevents Azure VMs from being exposed via a public IP address. A single device can be used for monitoring and logging. Similarly, we can restrict the IP addresses that can communicate with the Jump Box.

Integrating an ELK server allows users to easily monitor the vulnerable VMs for changes to the network and system system logs.

- What does Filebeat watch for?
  - Logs from Filebeat are collected, indexed, and forwarded to Elasticsearch or Logstash based on your specified files or locations.
 
- What does Metricbeat record?
  - You can ship metrics and statistics that are collected by Metricbeat to an output such as Elasticsearch or Logstash, as you specify.

The configuration details of each machine may be found below.
_Note: Use the [Markdown Table Generator](http://www.tablesgenerator.com/markdown_tables) to add/remove values from the table.

| Name     | Function      |          IP Address       | Operating System |
|----------|---------------|---------------------------|------------------|
| Jump Box | Gateway       | 10.1.0.4 / 20.85.232.68	 | Linux            |
| Web-1	   | UbuntuServer  | 10.1.0.5 / 40.114.124.48	 | Linux            |
| Web-2    | UbuntuServer  | 10.1.0.6 / 40.114.124.48	 | Linux            |
| DVWA-VM3 | UbuntuServer  | 10.1.0.7 / 40.114.124.48	 | Linux            |
| ELKserver| UbuntuServer  | 10.2.0.4 / 20.84.136.246	 | Linux            |

### Access Policies

The machines on the internal network are not exposed to the public Internet. 

Only the Jump-Box-Provisioner machine can accept connections from the Internet. Access to this machine is only allowed from the following IP addresses:

- My workstation public IP through TCP 5601.

Machines within the network can only be accessed by Workstation and Jump-Box-Provisioner through SSH Jump-Box.
- _TODO: Which machine did you allow to access your ELK VM? What was its IP address?_

A summary of the access policies in place can be found in the table below.

| Name     | Publicly Accessible | Allowed IP Addresses |
|----------|---------------------|----------------------|
| Jump Box | Yes/No              | 10.0.0.1 10.0.0.2    |
|          |                     |                      |
|          |                     |                      |

### Elk Configuration

Ansible was used to automate configuration of the ELK machine. No configuration was performed manually, which is advantageous because...
- _TODO: What is the main advantage of automating configuration with Ansible?_

The playbook implements the following tasks:
- _TODO: In 3-5 bullets, explain the steps of the ELK installation play. E.g., install Docker; download image; etc._
- ...
- ...

The following screenshot displays the result of running `docker ps` after successfully configuring the ELK instance.

![TODO: Update the path with the name of your screenshot of docker ps output](Images/docker_ps_output.png)

### Target Machines & Beats
This ELK server is configured to monitor the following machines:
- _TODO: List the IP addresses of the machines you are monitoring_

We have installed the following Beats on these machines:
- _TODO: Specify which Beats you successfully installed_

These Beats allow us to collect the following information from each machine:
- _TODO: In 1-2 sentences, explain what kind of data each beat collects, and provide 1 example of what you expect to see. E.g., `Winlogbeat` collects Windows logs, which we use to track user logon events, etc._

### Using the Playbook
In order to use the playbook, you will need to have an Ansible control node already configured. Assuming you have such a control node provisioned: 

SSH into the control node and follow the steps below:
- Copy the _____ file to _____.
- Update the _____ file to include...
- Run the playbook, and navigate to ____ to check that the installation worked as expected.

_TODO: Answer the following questions to fill in the blanks:_
- _Which file is the playbook? Where do you copy it?_
- _Which file do you update to make Ansible run the playbook on a specific machine? How do I specify which machine to install the ELK server on versus which to install Filebeat on?_
- _Which URL do you navigate to in order to check that the ELK server is running?

_As a **Bonus**, provide the specific commands the user will need to run to download the playbook, update the files, etc._
